/*jshint esversion: 6 */

var c = document.getElementById("woay");
var ctx = c.getContext("2d");

var circleLineColor = "#ff8144";
var colors = ['#f2f6f5', '#c8dad3', '#93b5b3', '#63707e'];
var pizzaCount = 8;

let {
    width,
    height
} = c;

function downloadAsImage() {}

function pickColor() {}

function drawCircle(ctx, x, y, r, angle, startAngle) {
    startAngle = startAngle ? startAngle : 0;
    ctx.beginPath();
    ctx.arc(x, x, r, startAngle, startAngle + angle * Math.PI / 180);
    ctx.closePath();
    ctx.stroke();
}


function drawFilledCircle(ctx, x, y, r1, r2) {
    let startAngle = 0;
    let angle = 360;

    ctx.fillStyle = circleLineColor;
    ctx.beginPath();
    ctx.arc(x, y, r2, startAngle, startAngle + angle * Math.PI / 180);
    ctx.closePath();
    ctx.fill();

    ctx.fillStyle = colors[0];
    ctx.beginPath();
    ctx.arc(x, y, r1, startAngle, startAngle + angle * Math.PI / 180);
    ctx.closePath();
    ctx.fill();
}


function drawPizza(ctx, x, y, r, lineCount, k) {
    let angle = (2 * Math.PI) / lineCount;
    let start = k * angle;
    start -= (Math.PI / 2 + angle / 2);
    ctx.beginPath();
    ctx.arc(x, y, r, start, start + angle);
    ctx.lineTo(x, y);
    ctx.fill();

    // TODO: custom text color
    ctx.fillStyle = "#000";
    ctx.font = "bold " + r / lineCount * 1.6 + "px serif";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    let center = (start + Math.PI / 2) + 0.5 * angle;

    ctx.translate(x, y);

    // TODO: has option: vertical or horizontal align text
    ctx.rotate(center);
    ctx.fillText(k + 1, 0, -r * 0.62); // TODO: fill text or picture instead of (k+1)

    ctx.rotate(-center);
    ctx.translate(-x, -y);

}

function render() {
    const [x, y] = [width / 2, height / 2];

    let r1 = width / 2 - 21;
    let r2 = r1 + 20;
    drawFilledCircle(ctx, x, y, r1, r2, 360);
    for (let i = 0; i < pizzaCount; i++) {
        let color = i % colors.length;
        //Check nếu số mảnh chia cho số màu dư 1, và nếu mảnh đang vẽ là mảnh cuối 
        if ((pizzaCount % colors.length == 1) && (i == pizzaCount - 1)) {
            color = 2;
        }
        ctx.fillStyle = colors[color];
        drawPizza(ctx, x, y, r1, pizzaCount, i);
    }
}

let template = $('#tpl-palette').html();

for (let i = 0; i < 6; i++) {
    let color = popularColors[i];
    let {
        code
    } = color;
    let html = template;
    let colors = parseCodeToColors(code);
    while (color = colors.shift()) {
        html = html.replace('{{COLOR}}', color);
    }
    html = html.replace('{{ID}}', i);
    $('#color-selector').append(html);
}

$(document).on('click', '.palette', function () {
    let id = $(this).attr('id');
    id = id.split('-').pop() - 0;
    colors = parseCodeToColors(popularColors[id].code);
    render();
});


$(document).on('click', '#btn-pizza-count button', function () {
    let value = $(this).text() - 0;
    pizzaCount = value;
    $(this).addClass('btn-primary').removeClass('btn-secondary')
        .siblings().removeClass('btn-primary').addClass('btn-secondary');
    render();
});

function parseCodeToColors(code) {
    let output = [];
    for (let i = 0; i < 4; i++) {
        let color = code.substr(i * 6, 6);
        output.push('#' + color);
    }
    return output;
}


render();
var angle = 0;



function downloadAsImage() {
    var download = document.getElementById("btn-save");
    var image = document.getElementById("woay").toDataURL("image/png");
    download.setAttribute("href", image);
}

$('#btn-save').click(function () {
    downloadAsImage();
    localStorage.setItem(c.id, c.toDataURL());
    localStorage.setItem("pizzaCount", pizzaCount);
});