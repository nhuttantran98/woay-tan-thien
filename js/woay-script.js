/*jshint esversion: 6 */
var c = document.getElementById("woay");
var ctx = c.getContext("2d");
let {
    width,
    height
} = c;
var Tempfullname;
var Tempphone;
var img = document.getElementById("wheel-img");
    if (imgLoaded(img)) {
        ctx = c.getContext("2d");
        console.log(img.width,img.height);
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, width, height);
    } else {
        img.onload = function () {
            c.width = img.width;
            c.height = img.height;
            ctx = c.getContext("2d");
            ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, width, height);
        };
    }

function imgLoaded(imgElement) {
    return imgElement.complete && imgElement.naturalHeight !== 0;
  }
  

function adjustWoayButton() {
    let ctnWoayWidth = $(".btn-container").width();
    let ctnWoayHeight = $(".btn-container").height();

    let btnWoayWidth = $(".btn-woay").width();
    let btnWoayHeight = $(".btn-woay").height();
    let btnWoayTop = (ctnWoayHeight - btnWoayHeight) / 2;
    let btnWoayLeft = (ctnWoayWidth - btnWoayWidth) / 2;

    $(".btn-woay").css({
        "display": "block",
        "top": `${btnWoayTop}px`,
        "left": `${btnWoayLeft}px`
    });

}

function adjustWoayArrow() {
    let ctnWoayWidth = $(".btn-container").width();
    let ctnWoayHeight = $(".btn-container").height();

    let arrowWoayWidth = $(".woay-arrow").width();
    let arrowWoayLeft = (ctnWoayWidth - arrowWoayWidth) / 2;

    $(".woay-arrow ").css({
        "display": "block",
        "left": `${arrowWoayLeft}px`
    });
}


adjustWoayButton();
adjustWoayArrow();
$(window).resize(adjustWoayButton);
$(window).resize(adjustWoayArrow);

function loadPopUp(reward){
    $('.title-big').html("");
    let rewardTitle= reward.title;
    $('.title-big').append(rewardTitle);
    $('.input-code-x').val(reward.serial);
    $(document).find("#w-award").fadeIn();
}
function closePopup(){
    $(document).find("#w-award").fadeOut();
    // $(document).find("#myPopup-award").fadeIn();
    $(document).find("#products").fadeIn();
    let elem=$(document).find("#products");
    $('html, body').stop().animate({
        scrollTop: elem.offset().top
    }, 1000);

}
function angleToPosition(angle, pizzaCount) {
    let anglePerPizza = (2 * Math.PI) / pizzaCount;
    // let ang = angle - Math.PI / 2;
    let ang = angle + anglePerPizza / 2;
    let r = ang / anglePerPizza;
    let oldR = r;
    while (r > pizzaCount) {
        r -= pizzaCount;
    }
    return r;
}

function rotatePoint(x, y, angle) {
    ctx.translate(x, y);
    ctx.rotate(-angle);
    ctx.translate(-x, -y);
}

function rewardHandler(number, reward) {
    if (reward.type == 'fail') {
        alert(`[${number}] Chúc bạn may mắn lần sau`);
    } else {
        loadPopUp(reward);
        
    }
}

let totalAngle = 0;
let reward = false;

let randomNumber = 1;
let minNumber = 0;
let maxNumber = 0;


function getReward() {
    setTimeout(_ => {
        reward = fakeGetReward();
        randomNumber = getRewardIndex(reward) + 1;
        console.log("randomNumber:", randomNumber);
        minNumber = randomNumber - 0.5 - (Math.random() * 0.3);
        maxNumber = minNumber + 0.8;
    }, 2500);
}

let userInfo = {};



$('#btn-woay').click(function () {
    if (!userInfo.fullname) {
        let fullname = prompt('Nhap ten a');
        userInfo.fullname = fullname;
        getReward();
        return;
    }
    const [x, y] = [width / 2, height / 2];
    let speed = 2;
    let angle0 = totalAngle;
    let duration = 3500;
    let start = performance.now();
    let oldAngle = 0.3;
    let stopAngle = 0.01;
    function getRate() {
        if (oldAngle > 0.25) return 0.85;
        if (oldAngle > 0.2) return 0.90;
        if (oldAngle > 0.15) return 0.92;
        if (oldAngle > 0.1) return 0.94;
        if (oldAngle > 0.05) return 0.96;
        if (oldAngle <= stopAngle) return 1;
        
        return 0.98;
    }
   
    function frame() {
        let time = performance.now() - start;
        // let oldT = time / duration;

        // let t = ~~(oldT * 1000) / 1000;
        let t = time / duration;
        let newAngle = angle0 + (speed * t - 0.5 * speed * t * t) * 100;
        let angle = newAngle - totalAngle;

        // điều chỉnh
        if (angle < oldAngle) {
            console.log('---->', getRate());
            angle = oldAngle * getRate();
            oldAngle = angle;
            newAngle = totalAngle + angle;
        }

        totalAngle = newAngle;
        let currentPosition = angleToPosition(totalAngle, pizzaCount);

        console.log(t, text6(angle));
        
        if (angle <= stopAngle && shouldStop(minNumber, maxNumber, currentPosition)) {
            return rewardHandler(randomNumber, reward);
        } else {
            rotatePoint(x, y, angle);
            ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, width, height);
            window.requestAnimationFrame(frame);
        }
    }

    window.requestAnimationFrame(frame);
});

$('#button').click(function () {
    if ($('.signin-form [name="nameSignin"]').val()==="") {
        $('#myModalLogin').modal("show");
        Tempfullname = $('.signin-form [name="nameSignin"]').val();
        userInfo.fullname = Tempfullname;
        getReward();
        return;
    }
    
    $("#btn-wheel").click();
    Tempfullname = $('.signin-form [name="nameSignin"]').val();
    Tempphone = $('.signin-form [name="phoneSignin"]').val();
    $(".customer-checkout-form [name='name']").val(Tempfullname);
    $(".customer-checkout-form [name='phone']").val(Tempphone);
    const [x, y] = [width / 2, height / 2];
    let speed = 2;
    let angle0 = totalAngle;
    let duration = 3500;
    let start = performance.now();
    let oldAngle = 0.3;
    let stopAngle = 0.01;

    function getRate() {
        if (oldAngle > 0.25) return 0.85;
        if (oldAngle > 0.2) return 0.90;
        if (oldAngle > 0.15) return 0.92;
        if (oldAngle > 0.1) return 0.94;
        if (oldAngle > 0.05) return 0.96;
        if (oldAngle <= stopAngle) return 1;
        
        return 0.98;
    }

    function frame() {
        let time = performance.now() - start;
        // let oldT = time / duration;

        // let t = ~~(oldT * 1000) / 1000;
        let t = time / duration;
        let newAngle = angle0 + (speed * t - 0.5 * speed * t * t) * 100;
        let angle = newAngle - totalAngle;

        // điều chỉnh
        if (angle < oldAngle) {
            console.log('---->', getRate());
            angle = oldAngle * getRate();
            oldAngle = angle;
            newAngle = totalAngle + angle;
        }

        totalAngle = newAngle;
        let currentPosition = angleToPosition(totalAngle, pizzaCount);

        console.log(t, text6(angle));
        
        if (angle <= stopAngle && shouldStop(minNumber, maxNumber, currentPosition)) {
            return rewardHandler(randomNumber, reward);
        } else {
            rotatePoint(x, y, angle);
            ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, width, height);
            window.requestAnimationFrame(frame);
        }
    }

    window.requestAnimationFrame(frame);
});

function shouldStop(minNumber, maxNumber, currentPosition) {
    return minNumber < currentPosition &&
        maxNumber > currentPosition;
}
function text6(n) {
    return n.toString().substr(0, 6 + 2); // '0.' = 2 character
}
