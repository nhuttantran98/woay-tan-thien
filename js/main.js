if ($('.home-slider').length > 0) {
    $('.home-slider').owlCarousel({
        loop: true,
        // autoplay: true,
        // autoplayTimeout: 4000,
        margin: 0,
        responsiveClass: true,
        items: 1,
        dots: true,
        navText: ["", ""],
        rewindNav: true,
        nav: true,
    })
}

if ($('.slider-owl-2').length > 0) {
    $('.slider-owl-2').owlCarousel({
        loop: false,
        margin: 0,
        responsiveClass: true,
        items: 4,
        dots: true,
        nav: false,
        responsive: {
            // breakpoint from 0 up
            0: {
                items: 1,
            },
            480: {
                items: 1,
            },
            768: {
                items: 1,
            },
            1200: {
                items: 4,
            }
        }
    })
}
if ($('.slider-owl-3').length > 0) {
    $('.slider-owl-3').owlCarousel({
        loop: false,
        margin: 0,
        responsiveClass: true,
        items: 4,
        dots: true,
        nav: false,
        responsive: {
            // breakpoint from 0 up
            0: {
                items: 1,
            },
            480: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 2,
            },
            1200: {
                items: 4,
            }
        }
    })
}

if ($('.m-home-slider').length > 0) {

    $('.m-home-slider').owlCarousel({
        loop: true,
        // autoplay: true,
        // autoplayTimeout: 4000,
        margin: 0,
        responsiveClass: true,
        items: 1,
        dots: true,
        navText: ["", ""],
        rewindNav: true,
        nav: true,

    })
}

$(document).ready(function () {
    // if ($(window).width() < 992) {
    $('.header-mobile .icon-menu').click(function () {
        $('nav#menu').mmenu({
            "extensions": [
                "position-back",
                "position-right",
                "fullscreen"
            ]
        });

    })
    // }

    // Menu fixed
    $(window).scroll(function () {
        if ($(document).scrollTop() > 100) {
            $('#menu-center').addClass('active');
        } else {
            $('#menu-center').removeClass('active');
        }
    });

    $(document).on("scroll", onScroll);

    //smoothscroll
    $('.menu-scroll a[href^="#"].link-scroll').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $("html, body").animate({
            scrollTop: $target.offset().top - 50
        });

    });

    function onScroll(event) {
        var scrollPos = $(document).scrollTop();
        // $('#menu-center a').each(function () {
        //     var currLink = $(this);
        //     var refElement = $(currLink.attr("href"));
        //     if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
        //         $('#menu-center ul li a').removeClass("active");
        //         currLink.addClass("active");
        //     } else {
        //         currLink.removeClass("active");
        //     }
        // });
    }
    onScroll();

    // Click Menu mobile
    /*$('#menu li a').click(function(e) {
        if($(this).hasClass('link-scroll')) {
            var API = $("#menu").data( "mmenu" );
            API.close();
        } else {
           
            var self = jQuery(this);
            var href = self.attr('href');
            e.preventDefault();
            window.location = href;
        }
    })*/

    // Readmore section 4
    $('.btn-readmore').click(function () {
        $(this).closest('.section-4').toggleClass('show-all');
        // if($('.section-4').hasClass('show-full')) {
        //     $(this).text("TÌM HIỂU CHI TIẾT");
        // } else {
        //     $(this).text("THU GỌN");
        // }
    })



})

// Firework 
var cx = 0,
    cy = 0;

var color1 = new Array(new Array({
        r: 241,
        g: 0,
        b: 0
    }, {
        r: 255,
        g: 72,
        b: 72
    }, {
        r: 239,
        g: 107,
        b: 107
    }, {
        r: 255,
        g: 191,
        b: 191
    }, {
        r: 255,
        g: 255,
        b: 255
    }),
    new Array({
        r: 12,
        g: 152,
        b: 228
    }, {
        r: 81,
        g: 188,
        b: 246
    }, {
        r: 100,
        g: 195,
        b: 247
    }, {
        r: 195,
        g: 232,
        b: 252
    }, {
        r: 255,
        g: 255,
        b: 255
    }),
    new Array({
        r: 202,
        g: 228,
        b: 12
    }, {
        r: 226,
        g: 246,
        b: 81
    }, {
        r: 229,
        g: 247,
        b: 100
    }, {
        r: 245,
        g: 252,
        b: 195
    }, {
        r: 255,
        g: 255,
        b: 255
    }));
var color2 = new Array(new Array({
        r: 255,
        g: 0,
        b: 0
    }, {
        r: 228,
        g: 225,
        b: 0
    }, {
        r: 169,
        g: 0,
        b: 231
    }, {
        r: 255,
        g: 0,
        b: 148
    }, {
        r: 255,
        g: 255,
        b: 255
    }),
    new Array({
        r: 0,
        g: 153,
        b: 231
    }, {
        r: 194,
        g: 0,
        b: 231
    }, {
        r: 255,
        g: 118,
        b: 0
    }, {
        r: 141,
        g: 227,
        b: 0
    }, {
        r: 255,
        g: 255,
        b: 255
    }),
    new Array({
        r: 255,
        g: 251,
        b: 125
    }, {
        r: 0,
        g: 206,
        b: 223
    }, {
        r: 247,
        g: 154,
        b: 255
    }, {
        r: 255,
        g: 165,
        b: 106
    }, {
        r: 255,
        g: 255,
        b: 255
    }));
var color3 = new Array(new Array({
        r: 0,
        g: 94,
        b: 226
    }, {
        r: 255,
        g: 255,
        b: 255
    }),
    new Array({
        r: 231,
        g: 234,
        b: 0
    }, {
        r: 255,
        g: 255,
        b: 255
    }),
    new Array({
        r: 255,
        g: 109,
        b: 0
    }, {
        r: 255,
        g: 255,
        b: 255
    }));
var color4 = new Array(
    new Array({
        r: 232,
        g: 255,
        b: 0
    }),
    new Array({
        r: 232,
        g: 255,
        b: 0
    }),
    new Array({
        r: 232,
        g: 255,
        b: 0
    }),
    new Array({
        r: 232,
        g: 255,
        b: 0
    }),
    new Array({
        r: 232,
        g: 255,
        b: 0
    }),
    new Array({
        r: 232,
        g: 255,
        b: 0
    }),
    new Array({
        r: 232,
        g: 255,
        b: 0
    }),
    new Array({
        r: 232,
        g: 255,
        b: 0
    }),
    new Array({
        r: 232,
        g: 255,
        b: 0
    })
);


jQuery(document).ready(function () {
    // Fireworks
    $(document).on('show.bs.modal', '.modal-fireworks', function () {
        $('.fireworks').firemaks({
            color: color4,
            type: 2,
            color_child: 'inherit',
            type_child: 'inherit',
            boom_count: 500,
            boom_time_from: 0.2
        });
    });

    // Link Popup
    var qrStr = window.location.search;
    var spQrStr = qrStr.substring(1);
    var arrQrStr = new Array();
    var arr = spQrStr.split('&');

    for (var i = 0; i < arr.length; i++) {
        var queryvalue = arr[i].split('=');
        var id = queryvalue[0];
        var comment = queryvalue[1];

        //document.write("id: "+id+" comment: "+comment+"<br/>");
        if (id == "kiemtraphanthuong") {
            $('#myModal7').modal('show');
        } else {
            console.log("False");
        }
    }

    $('#menu .link-section').click(function () {
        var name_section = $(this).data('section');
        $('.section-mobile').removeClass('active');
        $('.section-mobile#' + name_section).addClass('active');
        // closeMmenu();
        // return false;
        var API = $("#menu").data("mmenu");
        API.close();
        removeActive();
    });

    $('#menu .link-popup').click(function () {
        var API = $("#menu").data("mmenu");
        API.close();
        removeActive();
        setTimeout(function () {
            $('#myModal7').modal('show');
        }, 300);
    });

 

    function runHorse() {
        setTimeout(function () {
            $('.horse-animation').addClass('active');
        }, 800);
        setTimeout(function () {
            $('.horse-animation-2').addClass('active');
        }, 2200);
    }

    function removeActive() {
        $('.blind-left').removeClass('active');
        $('.blind-right').removeClass('active');
        $('.horse-animation').removeClass('active');
        $('.horse-animation-2').removeClass('active');
    }

    function wheel_mobile() {
        function first() {
            $('.blind-left').addClass('active');
            $('.blind-right').addClass('active');
        }

        function second() {
            runHorse();
        }

        function third() {
            setTimeout(function () {
                $('#myModal2').modal('show');
            }, 3800);
        }
        $.when(first()).done(second()).done(third());
    }

    function wheel_desktop() {
        function first() {
            $('.blind-left').addClass('active');
            $('.blind-right').addClass('active');
        }
        // function second() {
        //     runHorse();
        // }
        function third() {
            setTimeout(function () {
                $('#myModal2').modal('show');
            }, 900);
        }
        $.when(first()).done(third());
    }

    // JS click button Wheel 
    if ($(window).width() > 767) {
        $('.btn-wheel').click(function () {
            if ($('#modalAward_demo').hasClass('in')) {
                $('#modalAward_demo').modal('hide');
                setTimeout(function () {
                    wheel_desktop();
                }, 700);
            } else {
                wheel_desktop();
            }

        });
    } else {
        $('.btn-wheel').click(function () {
            if ($('#modalAward_demo').hasClass('in')) {
                $('#modalAward_demo').modal('hide');
                setTimeout(function () {
                    wheel_mobile();
                }, 700);
            } else {
                wheel_mobile();
            }

        });
    }


    // Audio Wheel
    function audioWheel() {
        $("#audio_wheel")[0].preload = "auto";
        $("#audio_wheel")[0].play();
        $("#audio_wheel")[0].loop = true;
    }
    $('.btn-wheel').click(function () {
        audioWheel();
    })
    $('#myModal2').on('hidden.bs.modal', function () {
        $("#audio_wheel")[0].pause();
        $("#audio_wheel")[0].currentTime = 0;
    })

    // $('#modalAward_50k').modal('show');
    // Modal first
    // $('#modalFirst').modal('show');

});


// Btn copy code
function copyToClipboard(el) {
    // resolve the element
    el = (typeof el === 'string') ? document.querySelector(el) : el;

    // handle iOS as a special case
    if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {

        // save current contentEditable/readOnly status
        var editable = el.contentEditable;
        var readOnly = el.readOnly;

        // convert to editable with readonly to stop iOS keyboard opening
        el.contentEditable = true;
        el.readOnly = true;

        // create a selectable range
        var range = document.createRange();
        range.selectNodeContents(el);

        // select the range
        var selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        el.setSelectionRange(0, 999999);

        // restore contentEditable/readOnly to original state
        el.contentEditable = editable;
        el.readOnly = readOnly;
    } else {
        el.select();
    }

    // execute copy command
    document.execCommand('copy');

}

$('.btn-copycode').click(function () {
    $(this).text("Copy thành công");
    $(this).css('background-color', '#828282');
    $(this).css('color', '#fff');
});
function openProduct(){
    closePopup();
    $('section-3').css('display','block');
    render();
}