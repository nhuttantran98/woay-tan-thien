/*jshint esversion: 6 */


const rewardSkus = rewards.map(x => x.sku);
const rewardCount = rewards.length;

//TODO: Implement get reward from server via ajax
function fakeGetReward() {
    let pos = ~~(Math.random() * rewardCount);
    return Object.assign({}, rewards[pos], {
        serial: makeid(8)
    });
}

function getRewardIndex(reward) {
    return rewardSkus.indexOf(reward.sku);
}

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}