var cartItemTemplate =
    `<li class="view-cart-product">
          <div>
              <span class="subtr"><button class="btn-sub-add" onclick="removeFromCart({{i}})"><i class="fa fa-minus" aria-hidden="true"></i></button></span>
              <span class="box-number">{{AMOUNT}}</span>
              <span class="add"><button class="btn-sub-add" onclick="addToCart({{i}})"><i class="fa fa-plus" aria-hidden="true"></i></button></span>
              <span class="box-cart-item item-name">{{TITLE}}</span><span class="cart-price">{{TOTALMONEY}} &#8363;</span>
          </div>
      </li>`;
var cartMenuB =
    `<li class="view-cart-product">
          <div> 
              <span class="box-number">{{AMOUNT}}</span>
              <span class="box-cart-item item-name">{{TITLE}}</span><span class="cart-price">{{TOTALMONEY}} &#8363;</span>
          </div>
      </li>`;
var cartItemTotal =
    `<div class="total-item-cart">
        <span class=" item-name ">SubTotal ({{COUNT}} món)</span>
        <span class="price-total">{{TOTAL}} &#8363;</span>
    </div>
    <div class="total-item-cart" id="total-item-cart-discount">
        <span class="item-name">Discount</span>
        <span class="price-total">{{REWARD}}</span>
    </div>
    <div class="total-money">
        <span class="item-name">Total</span>
        <span class="price-total">{{TOTALMONEYX}} &#8363;</span>
    </div>`;
var cartItemHeader =
    `<li class="view-cart-product-center">
    <div class="box-cart-item-header"> 
        <div class="img-cart-header">
            <img alt="" src="{{IMAGE}}"></a>
        </div>
        <div class="box-name-cart-header">
            <div class="text-name-cart-header">
                <span class="box-cart-item item-name">{{TITLE}}</span>
            </div>
            <div class="box-mount">
                <span class="subtr"><button class="btn-sub-add" onclick="removeFromCart({{i}})"><i class="fa fa-minus" aria-hidden="true"></i></button></span>
                <span class="box-number">{{AMOUNT}}</span>
                <span class="add"><button class="btn-sub-add" onclick="addToCart({{i}})"><i class="fa fa-plus" aria-hidden="true"></i></button></span>
                <span class="cart-price">{{TOTALMONEY}} &#8363;</span>
            </div>
        </div>
    </div>
</li>`;
var cartItemTotalHeader =
    `<div class="total-item-cart">
        <span class=" item-name ">SubTotal ({{COUNT}} món)</span>
        <span class="price-total">{{TOTAL}} &#8363;</span>
    </div>
    <div class="total-item-cart" id="total-item-cart-discount">
        <span class="item-name">Discount</span>
        <span class="price-total">{{REWARD}}</span>
    </div>
    <div class="total-money">
        <span class="item-name">Total</span>
        <span class="price-total">{{TOTALMONEYX}} &#8363;</span>
    </div>`;

var cart = [];
let state = "order";
var orderInfo = {};
var rewardDiscount=0;
var totalx=0;
var formatMoney = (n) => n.toLocaleString().replace(/,/g, '.');

render();

$(document).on("click", "#btn-buy-book", function (e) {

    let name = $(".customer-checkout-form [name='name']").val();
    let phone = $(".customer-checkout-form [name='phone']").val();
    let address = $(".customer-checkout-form [name='address']").val();
    let email = $(".customer-checkout-form [name='email']").val();
    let method_shipping = $(".customer-checkout-form [name='method-shipping']:checked").val();

    if (validateNameAndAddress(name, address) == false) {
        return;
    }
    if (validateEmail(email) == false) {
        return;
    }
    if (validatePhone(phone) == false) {
        return;
    }
    if (validateReceiveNameAndAddress(name, address) == false) {
        return;
    }
    if (validateReceivePhone(phone) == false) {
        return;
    }
    orderInfo = {
        name, phone, address, email, method_shipping
    }
    submitCart();
    $("#cartHeader").fadeOut();
    $('#exampleModal').modal('hide')
    Swal.fire(
        'Order Success!',
        'Continued to order!',
        'success',
    ).then();
    state = "order";
    cart = [];
    render();
});

function render() {
    if (cart.length == 0) {
        stage = "order";
        $("#btn-buy").attr("disabled", true);
    } else {
        $("#btn-buy").attr("disabled", false);
    }
    switch (state) {
        case "order":
            $(".form-content").hide();
            $(".box-content").show();
            $(".view-cart button").text("Xem giỏ hàng");
            break;
        case "checkout":
            $(".form-content").show();
            $(".box-content").hide();
            $(".view-cart button").text("Đặt hàng");
            break;
        default:
            break;
    }


    $('.item-menu').html("");
    $('.view-cart-menu').html("");
    $('.view-cart-menu-b').html("");
    $('.view-cart-menu-header').html("");
    $('.view-cart-header-sub').html("");
    var count = 0, total = 0, sale = 30000;


    for (let i = 0; i < cart.length; i++) {
        let cartItem = cart[i];
        let product = products.find(p => p.id == cartItem.id);
        let totalMoney = cartItem.amount * product.price;
        count += cartItem.amount;
        total += totalMoney;
        let html1 = cartItemTemplate
            .replace('{{TITLE}}', product.title)
            .replace('{{AMOUNT}}', cartItem.amount)
            .replace('{{TOTALMONEY}}', formatMoney(totalMoney))
            .replace('{{i}}', i)
            .replace('{{i}}', cartItem.id);
        let html2 = cartMenuB
            .replace('{{TITLE}}', product.title)
            .replace('{{AMOUNT}}', cartItem.amount)
            .replace('{{TOTALMONEY}}', formatMoney(totalMoney));
        let html3 = cartItemHeader
            .replace('{{TITLE}}', product.title)
            .replace('{{AMOUNT}}', cartItem.amount)
            .replace('{{TOTALMONEY}}', formatMoney(totalMoney))
            .replace('{{i}}', i)
            .replace('{{i}}', cartItem.id)
            .replace('{{IMAGE}}', "/uploads/" + product.image)
        $('.view-cart-menu').append(html1);
        $('.view-cart-menu-b').append(html2);
        $('.view-cart-menu-header').append(html3);

    }


    rewardDiscount = 0;
    if (typeof reward != 'undefined') {
        switch (reward.sku) {
            case "VOUCHER20%":
                rewardDiscount = -total * 0.2;
                break;
            case "VOUCHER40%":
                rewardDiscount = -total * 0.4;
                break;
            case "VOUCHER30%":
                rewardDiscount = -total * 0.3;
                break;
            case "COUPON10%":
                rewardDiscount = -total * 0.1;
                break;
            case "GIAY":
                rewardDiscount = "Giày";
                break;
            case "VOUCHER10%":
                rewardDiscount = -total * 0.1;
                break;
            default:
                break;
        }
        if (rewardDiscount != "Giày") {
            totalx = total + rewardDiscount;
            rewardDiscount = formatMoney(rewardDiscount);
        } else {
            totalx = total;
        }

        $('.view-cart-menu').append(cartItemTotal
            .replace('{{COUNT}}', count)
            .replace('{{TOTAL}}', formatMoney(total))
            .replace('{{TOTALMONEYX}}', formatMoney(totalx))
            .replace('{{REWARD}}', rewardDiscount)
        );
        $('.view-cart-menu-b').append(cartItemTotal
            .replace('{{COUNT}}', count)
            .replace('{{TOTAL}}', formatMoney(total))
            .replace('{{TOTALMONEYX}}', formatMoney(totalx))
            .replace('{{REWARD}}', rewardDiscount)
        );
        $('.view-cart-header-sub').append(cartItemTotal
            .replace('{{COUNT}}', count)
            .replace('{{TOTAL}}', formatMoney(total))
            .replace('{{TOTALMONEYX}}', formatMoney(totalx))
            .replace('{{REWARD}}', rewardDiscount)
        );
    }
}

function removeFromCart(index) {
    for (let i = 0; i < cart.length; i++) {
        let temp = cart[i];
        if (i == index && temp.amount > 0) temp["amount"]--;
        if (temp["amount"] == 0) cart.splice(i, 1);
    }
    render();
}


function addToCart(index) {
    var found = false;
    for (let item of cart) {
        if (item.id == index) {
            item.amount++;
            found = true;
            toastr.success('Cập nhật giỏ hàng thành công!');
        }
    }
    if (!found) {
        cart.push({
            id: index,
            amount: 1
        });
        $('#cart-menu-btn a').css('cursor', 'pointer');
        toastr.success('Thêm vào giỏ hàng thành công!');
    }

    // let item = cart.findIndex(item =>item.id = index)
    //   if(item != -1){
    //    item['amount']++;
    //   }else{
    //     cart.push({
    //       id: index,
    //       amount: 1
    //     });
    //   }

    render();
}

// function apdung(){
//   let coupon = $(".wrap-input [name='maCoupon']").val();
//   for(let i = 0; i < coupons.length; i++){
//     if(coupons[i].ma = coupon)
//     let giamgia  = coupons[i].sub;
//   }

// }
function addItem(index) {
    cart[index].amount++;
    render();
}

function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true)
    } else {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Invalid Email!',
        })
        return (false)
    }
}

function validatePhone(phone) {

    if (/((09|03|07|08|05)+([0-9]{8})\b)/g.test(phone)) {
        return true;
    } else {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Số điện thoại khách hàng không hợp lệ!',
        })
        return false;
    }
}

function validateReceivePhone(receive_phone) {

    if (/((09|03|07|08|05)+([0-9]{8})\b)/g.test(receive_phone)) {
        return true;
    } else {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Số điện thoại nhận hàng không hợp lệ!',
        })
        return false;
    }
}

function validateNameAndAddress(name, address) {
    if (name == "") {
        Swal.fire({
            text: "Vui lòng nhập tên khách hàng!",
            type: 'error',
            title: 'Oops',
        })
        return false;
    } else if (address == "") {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Vui lòng nhập địa chỉ khách hàng!',
        })
        return false;
    } else {
        return true;
    }
}

function validateReceiveNameAndAddress(receive_name, receive_address) {
    if (receive_name == "") {
        Swal.fire({
            text: "Vui lòng nhập tên người nhận!",
            type: 'error',
            title: 'Oops',
        })
        return false;
    } else if (receive_address == "") {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Vui lòng nhập địa chỉ người nhận!',
        })
        return false;
    } else {
        return true;
    }
}

function submitCart() {
    let message = '';
    message += '--ĐƠN ĐẶT HÀNG--\n'
    message += "Tên người nhận: " + orderInfo.name + '\n';
    message += "Số điện thoại nhận hàng: " + orderInfo.phone + '\n';
    message += "Mail: " + orderInfo.email + '\n';
    message += "Địa chỉ nhận hàng: " + orderInfo.address + '\n';
    message += "Giao hàng: " + orderInfo.method_shipping + '\n';
    message += '--Sản phẩm--\n'
    let total = 0;
    for (let cartItem of cart) {
        let product = products.find(p => p.id == cartItem.id);
        let subtotal = product.price * cartItem.amount;
        message += `${product.title} x ${cartItem.amount} = ${formatMoney(subtotal)}đ\n`;
        total += subtotal;
    }
    message += "Discount: " + rewardDiscount + '\n';
    message += '---\n';
    message += `TOTAL: ${formatMoney(totalx)}đ`;

    $.post('https://api.telegram.org/bot820639914:AAFP634d-GeJ3ejuw2fZzFzWFqgzs3oZIT0/sendMessage', {
        chat_id: -218128997,
        text: message
    }, function () {
        // alert('Submit Cart', 'Mua hàng thành công', 'success');
    })
}
