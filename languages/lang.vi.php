<?php
$lang = array();
// URL

$lang['SEARCH_URL'] = '/tim-kiem';
$lang['CUSTOMER_URL'] = '/khach-hang';
$lang['LOGIN_URL'] = '/dang-nhap';
$lang['REGISTER_URL'] = '/dang-ky';
$lang['FORGOT_PASS_URL'] = '/customer/forgot-password';
$lang['COLLECTION_ALL_URL'] = '/nhom-san-pham/tat-ca';
$lang['BLOG_ALL_URL'] = '/nhom-bai-viet/tat-ca';
$lang['SEARCH_URL'] = '/tim-kiem';
$lang['LOGIN_URL'] = '/dang-nhap';
$lang['CONTACT'] = '/lien-he';
$lang['REGISTER_URL'] = '/dang-ky';
$lang['CUSTOMER_URL'] = '/khach-hang';
$lang['CUSTOMER_ORDER_URL'] = '/khach-hang/don-hang';
$lang['CUSTOMER_CHANGE_PASSWORD_URL'] = '/khach-hang/doi-mat-khau';
$lang['CUSTOMER_FORGOT_PASSWORD_URL'] = '/khach-hang/quen-mat-khau';
$lang['CUSTOMER_COUPON'] = '/khach-hang/ma-giam-gia';
$lang['WISHLIST_URL'] = '/san-pham-yeu-thich';
$lang['CART_URL'] = '/gio-hang';
$lang['CHECKOUT_URL'] = '/dat-hang';
$lang['CHECKOUT_SUCCESS_URL'] = '/dat-hang-thanh-cong';
$lang['ADDRESS_BOOK_URL'] = '/khach-hang/so-dia-chi';


//customer url
$lang['COUPON_URL'] = '/khach-hang/ma-khuyen-mai';
$lang['EDIT_ACCOUNT_URL'] = '/khach-hang';
$lang['MY_ORDER_URL'] = '/khach-hang/don-hang';
$lang['ADDERSS_BOOK_URL_2'] = '/khach-hang/so-dia-chi';
$lang['MY_WISHLIST_URL'] = '/khach-hang/san-pham-yeu-thich';




$lang['HOME'] = 'Trang chủ';
$lang['SUPPORT'] = 'Hỗ trợ';
$lang['CART'] = 'Giỏ hàng';
$lang['NICART'] = 'Không có sản phẩm trong giỏ hàng';
$lang['CHECKOUT'] = 'Đặt hàng';
$lang['CHECKOUTS'] = 'Đặt hàng thành công';
$lang['PRODUCT'] = 'Sản phẩm';
$lang['AMOUNT'] = 'Số lượng';
$lang['TOTAL'] = 'Tổng';
$lang['NAME'] = 'Họ và tên';
$lang['PHONE'] = 'Số điện thoại';
$lang['CITY'] = 'Tỉnh/Thành phố';
$lang['DISTRICT'] = 'Quận/Huyện';
$lang['ADDRESS'] = 'Địa chỉ';
$lang['CONTENT'] = 'Nội dung';
$lang['ERROR'] = 'Lỗi';
$lang['BTHOME'] = 'Về trang chủ';
$lang['SEARCHH'] = 'Nhập từ khóa cần tìm';
$lang['SERCATE'] = 'Danh Mục Dịch Vụ';
$lang['SUBSE'] = 'Nhập địa chỉ email của bạn';
$lang['SUBS'] = 'Đăng ký ngay!';
$lang['TITLE'] = 'Tiêu đề';
$lang['NAMET'] = 'Nhập tên của bạn';
$lang['EMAILT'] = 'Nhập email của bạn';
$lang['PHONET'] = 'Nhập số điện thoại của bạn';
$lang['CONTENTT'] = 'Nhập nội dung tin nhắn';
$lang['SEND'] = 'Gửi liên hệ';
$lang['TALKPRO'] = 'Hãy nói về dự án';
$lang['BY'] = 'Người viết';
$lang['BACK'] = 'Trở về';
$lang['SHARE'] = 'Chia sẻ';
$lang['CATEGORY'] = 'Danh mục';
$lang['SEENPOST'] = 'Bài viết nổi bật';
$lang['DEVBY'] = 'Phát triển bởi';
$lang['MOTFR'] = 'Thứ 2 - Thứ 6';
$lang['SATURDAY'] = 'Thứ 7';
$lang['SUNDAY'] = 'Chủ nhật';
$lang['MAILAT'] = 'Gửi mail qua';
$lang['CALLUS'] = 'Gọi chúng tôi';
$lang['SEARCH'] = 'Tìm kiếm';
$lang['STOTAL'] = 'Tạm tính';
$lang['SEEM'] = 'Xem thêm';
$lang['DAY'] = 'Ngày';
$lang['MONTH'] = 'Tháng';
$lang['YEAR'] = 'Năm';
$lang['SPLAN'] = 'Chọn gói';
$lang['ATCART'] = 'Thêm vào giỏ hàng';
$lang['ALL'] = 'Tất cả';
$lang['AVAIL'] = 'Trạng thái';
$lang['INSTOCK'] = 'Trong kho';
$lang['NOSTOCK'] = 'Hết hàng';
$lang['DESC'] = 'Mô tả ';
$lang['INFO'] = 'Thông tin';
$lang['COMM'] = 'Đánh giá';
$lang['SEENPRO'] = 'Sản phẩm đã xem';
$lang['HAVE'] = 'Có';
$lang['SEARCHR'] = 'kết quả tìm kiếm cho ';
$lang['SEARCHN'] = 'Không có kết quả tìm kiếm cho ';
$lang['SEARCHNONE'] = 'Hãy thử lại với từ khoá khác';
$lang['SEARCHNONE_FILTER'] = 'Hãy thử lại với bộ lọc khác';
$lang['SEARCHN_FILTER'] = 'Không có kết quả';
$lang['RESULTS'] = 'Kết quả tìm kiếm';

$lang['ABOUT_US'] = 'Về chúng tôi';
$lang['SEARCH'] = 'Tìm kiếm';
$lang['KEY_WORD'] = 'Từ khóa';
$lang['SEE_MORE'] = 'Xem thêm';
$lang['RESULT_COMPETITION'] = 'Kết quả tạm thời';
$lang['CATEGORY'] = 'Danh mục';
$lang['ADDRESS'] = 'Địa chỉ';
$lang['GET_INFO'] = 'Để lại thông tin liên hệ';
$lang['NAME'] = 'Họ tên';
$lang['EMAIL'] = 'Email';
$lang['PHONE'] = 'Số điện thoại';
$lang['MESSAGE'] = 'Tin nhắn';
$lang['SUBMIT'] = 'Gửi';
$lang['CONTACT_US'] = 'Liên hệ với chúng tôi';
$lang['INPUT_NAME'] = 'Vui lòng nhập tên!';
$lang['INPUT_ADDRESS'] = 'Vui lòng nhập địa chỉ!';
$lang['INPUT_EMAIL'] = 'Vui lòng nhập email!';
$lang['INPUT_PHONE'] = 'Vui lòng nhập số điện thoại!';
$lang['INPUT_REGION'] = 'Bạn chưa chọn Tỉnh/Thành phố';

$lang['INVALID_EMAIL'] = 'Email không hợp lệ!';
$lang['INVALID_PHONE'] = 'Số điện thoại không hợp lệ!';
$lang['EMPTY_CONTENT'] = 'Nội dung đang trống!';
$lang['CONTACT_EARLY'] = 'Thông tin đã được ghi nhận, chúng tôi sẽ liên hệ sớm nhất!';
$lang['HOME_PAGE'] = 'Trang chủ';
$lang['VIEW_NOW'] = 'Xem ngay';
$lang['VIEW_PRODUCT_DETAL'] = 'Xem chi tiết sản phẩm';
$lang['BUY_NOW'] = 'Mua ngay';
$lang['ADD_TO_CART'] = 'Thêm vào túi';
$lang['QUANTITY'] = 'Số lượng';
$lang['SIZE_CHART'] = 'Biểu đồ kích thước';
$lang['PAGE_CONTACT'] = 'Liên hệ';
// USER
$lang['USER_PAGE'] = 'Trang cá nhân';
$lang['USER_INFO'] = 'Thông tin cá nhân';
$lang['CHANGE_PASS'] = 'Đổi mật khẩu';
$lang['FULL_NAME'] = 'Họ tên';
$lang['PHONE'] = 'Điện thoại di động';
$lang['BIRTHDAY'] = 'NGÀY, THÁNG, NĂM SINH';
$lang['GENDER'] = 'Giới tính';
$lang['MALE'] = 'Nam';
$lang['FEMALE'] = 'Nữ';
$lang['OLD_PASS'] = 'Mật khẩu cũ';
$lang['NEW_PASS'] = 'Mật khẩu mới';
$lang['RE_PASS'] = 'Xác nhận mật khẩu';
$lang['BUTTON_SAVE'] = 'Lưu';
$lang['BUTTON_UPDATE'] = 'Cập nhật';
$lang['VIEW_BLOG'] = 'XEM BLOG';
$lang['SUBSCRIBE'] = 'ĐĂNG KÝ NHẬN TIN';
$lang['DOWNLOAD_APP'] = 'Tải ứng dụng';
$lang['REGISTER_EMAIL'] = 'Đăng ký';
$lang['PLACEHOLDER_SUBSCIBE_EMAIL'] = 'Nhập email của bạn';
$lang['CONNECT_US'] = 'Kết nối với chúng tôi';
$lang['COMPANY_INFO'] = 'Thông tin về công ty';
$lang['WE_ACCEPT'] = 'Chúng tôi đồng ý';
$lang['SHIPPING_PARTNER'] = 'Đối tác vận chuyển';
$lang['NOTI_PASS_OLD'] = 'Vui lòng nhập mật khẩu hiện tại';
$lang['NOTI_PASS_NEW'] = 'Vui lòng nhập mật khẩu mới!';
$lang['NOTI_PASS_VALIDATE'] = 'Mật khẩu bao mới phải gồm chữ số và kí tự!';
$lang['NOTI_PASS_PRE'] = 'Mật khẩu mới không trùng khớp!';
$lang['NOTI_PASS_SUCCESS'] = 'Cập nhật mật khẩu thành công !';
$lang['NOTI_PASS_ERR'] = 'Cập nhật mật khẩu không thành công !';
// LOGIN
$lang['NOTI_LOGIN_SUCCESS'] = 'Đăng nhập thành công !';
$lang['NOTI_LOGIN_ERR'] = 'Đăng nhập không thành công !';
$lang['LOGIN_FB'] = 'Đăng nhập với facebook';
$lang['LOGIN_GG'] = 'Đăng nhập với google';
$lang['PASSWORD'] = 'Mật khẩu';
$lang['REMEMBER_PASS'] = 'Nhớ tài khoản';
$lang['FORGOT_PASS'] = 'Quên mật khẩu ?';
$lang['LOGIN'] = 'Đăng nhập';
$lang['NO_ACCOUNT'] = 'Chưa có tài khoản?';
$lang['HAVE_ACCOUNT'] = 'Đã có tài khoản?';
$lang['REGISTER'] = 'Đăng ký';

//header
$lang['ITEM_CART'] = 'SẢN PHẨM TRONG TÚI';
$lang['SUM_CART'] = 'Tổng';
$lang['VIEW_CART'] = 'Xem giỏ hàng';
$lang['CHECKOUT_CART'] = 'Thanh toán';
$lang['EMPTY_CART'] = 'Không có SẢN PHẨM TRONG TÚI';


//USER

$lang['EDIT_ACCOUNT'] = 'Chỉnh sửa tài khoản';
$lang['MY_ORDER'] = 'Đơn hàng của tôi';
$lang['ADDRESS_BOOK'] = 'Sổ địa chỉ';
$lang['WISHLIST'] = 'Danh sách yêu thích';
$lang['MY_COUPON'] = 'Mã khuyến mãi';
$lang['LOGOUT'] = 'Đăng xuất';

//collection

$lang['FOLLOW_US'] = 'Theo dõi chúng tôi';
$lang['SORT_TIME'] = 'Sắp xếp theo mới nhất';
$lang['SORT_SALE'] = 'Sắp xếp theo bán chạy nhất';
$lang['SORT_DESC'] = 'Sắp xếp theo giá cao - thấp';
$lang['SORT_ASC'] = 'Sắp xếp theo giá thấp - cao';

//product
$lang['SIZE'] = 'Kích cỡ';
$lang['COLOR'] = 'Màu sắc';
$lang['PARAMETER'] = 'Thông số';
$lang['WEIGHT'] = 'Cân nặng';
$lang['CLOTH'] = 'Vải';
$lang['SEASON'] = 'Mùa';
$lang['AGE'] = 'Độ tuổi';
$lang['STYLE'] = 'Phong cách';
$lang['PRICE'] = 'Giá';
$lang['CHANGE'] = 'Thay đổi';
$lang['REMOVE_CART'] = 'Bỏ khỏi giỏ hàng';



//CHECKOUT
$lang['ADD_TO_CART_SUCCESS'] = 'Sản phẩm đã được thêm vào giỏ hàng';
$lang['OUT_OF_STOCK'] = 'Hết hàng';
$lang['ADD_TO_CART_OUT'] = 'Đã vượt qua số lượng trong kho, vui lòng chọn lại số lượng sản phẩm hoặc chọn sản phẩm khác';
// $lang['COLOR'] = 'Màu sắc';

//WISHLIST
$lang['ADD_WISHLIST_SUCCESS'] = 'Thêm sản phẩm vào danh sách ưa thích thành công';
$lang['ADD_WISHLIST_ERROR'] = 'Người dung chưa đăng nhập!';
$lang['ADD_WISHLIST_EXIST'] = 'Sản phẩm đã tồn tại trong danh sách ưa thích!';

$lang['REMOVE_WISHLIST_SUCCESS'] = 'Xóa sản phẩm khỏi danh sách ưa thích thành công';
$lang['NO_WISHLIST_PRODUCT'] = 'Không có sản phẩm yêu thích';
// $lang['COLOR'] = 'Màu sắc';

//ORDER
$lang['SECURE_PAYMENT'] = 'Thanh toán an toàn';
$lang['SAVE'] = 'Lưu';
$lang['SAVE_INFORMATION'] = 'Lưu thông tin thành công!';
$lang['NAME_INFORMATION'] = 'Tên, email không được rỗng';


$lang['SHIPPING_ADDRESS'] = 'Địa chỉ nhận hàng';
$lang['REGION'] = 'Tỉnh / Thành phố';
$lang['SUBREGION'] = 'Quận / huyện';
$lang['WARDS'] = 'Phường / Xã';
$lang['DEFAULT_ADDRESS'] = 'Đặt làm địa chỉ mặc định';
$lang['PAYMENT_METHODS'] = 'Phương thức thanh toán';
$lang['ORDER_SUMMARY'] = 'Tóm tắt đơn hàng';
$lang['EDIT'] = 'Chỉnh sửa';
$lang['TRANSPORT_FEE'] = 'Phí vận chuyển';
$lang['EST_TIME'] = 'thời gian giao hàng dự kiến';


//SUCCESS
$lang['PURCHASE_SUCCESS'] = 'Mua hàng thành công';
$lang['YOUR_ORDER_ID'] = 'MÃ SỐ ĐƠN HÀNG CỦA BẠN LÀ';
$lang['OR_SEND_TO_EMAIL'] = 'Hoặc gửi email tới';
$lang['TO_SUPPORT'] = 'để được hỗ trợ';

$lang['PURCHASE_SUCCESS_CONTENT_1'] = 'Vipxchange rất vui thông báo đơn hàng';
$lang['PURCHASE_SUCCESS_CONTENT_2'] = 'đã được tiếp nhận và đang trong quá trình xử lý. Chi tiết đơn hàng đã được gửi vào địa chỉ email của bạn.';
$lang['PURCHASE_SUCCESS_CONTENT_2'] = 'đã được tiếp nhận và đang trong quá trình xử lý. Chi tiết đơn hàng đã được gửi vào địa chỉ email của bạn.';
$lang['PURCHASE_SUCCESS_CONTENT_3'] = 'Đơn hàng sẽ được giao trong 4 - 7 ngày làm việc, tuỳ theo địa điểm nhận hàng của bạn.';

$lang['YOUR_QUESTION'] = 'Bạn có câu hỏi cần giải đáp ?';
$lang['FREQUENTLY_ASKED_QUESTION'] = 'Câu hỏi thường gặp';
$lang['SHIPPING_POLICY'] = 'Chính sách vận chuyển';
$lang['DOWNLOAD_APP_TO_CHECK'] = 'Tải App Vipxchange để kiểm tra đơn hàng mọi lúc, mọi nơi';
$lang['GOTO_HOME_PAGE'] = 'Trở về trang chủ';




$lang['DETAIL'] = 'Xem thêm';
$lang['BLOG'] = 'Nhóm bài viết';
$lang['COMMENT'] = 'Bình luận';
$lang['VIEW_ALL'] = 'Xem tất cả';
$lang['SEND_COMMENT'] = 'Gửi bình luận';
$lang['COMMENT_SUCCESS'] = 'Gửi bình luận thành công';
$lang['COMMENT_ERROR'] = 'Gửi bình luận thất bại';
$lang['WRITE_YOUR_COMMENT'] = 'Viết bình luận của bạn';
$lang['PLEASE_WRITE_COMMENT'] = 'Vui lòng nhập bình luận';


$lang['CODE_ORDER'] = 'Mã số đơn hàng';
$lang['TIME_ORDER'] = 'Ngày đặt hàng';
$lang['STATUS_ORDER'] = 'Trạng thái';
$lang['TOTAL_MONEY'] = 'Tổng tiền';

$lang['COUPON'] = 'Mã khuyến mãi';


$lang['VALUE'] = 'Giá trị';
$lang['TIME_RECEIVE'] = 'Ngày nhận';
$lang['TIME_EXPIRED'] = 'Ngày hết hạn';


//STATUS ORDER
$lang['PROCESSING'] = 'Đang xử lý';
$lang['NEW'] = 'Mới';
$lang['CONFIRM'] = 'Đã xác nhận';
$lang['DONE'] = 'Hoàn thành';
$lang['CANCEL'] = 'Đã hủy';
$lang['RETURN'] = 'Hoàn trả';


//STATUS COUPON
$lang['ACTIVE'] = 'Chưa dùng';
$lang['EXPIRED'] = 'Hết hạn';
$lang['USED'] = 'Đã dùng';

//  COUPON

$lang['COUPON_SUCCESS'] = 'Nhập mã giảm giá thành công';
$lang['COUPON_ERR'] = 'Mã khuyến mãi không đúng hoặc hết hạn!';
$lang['CANCEL_COUPON_SUCCESS'] = 'Hủy mã khuyến mãi thành công!';
$lang['ORDER_NOW'] = 'Tiến hành đặt hàng';
$lang['APPLY'] = 'Áp dụng';
$lang['COUPON_USER'] = 'Đang sử dụng mã';
$lang['COUPON_CANCLE'] = 'Hủy mã giảm giá';
$lang['COUPON_PLACE_HOLDER'] = '1 mã giảm giá / Mỗi order';
$lang['ORDER_SUMMARY'] = 'Tóm tắt đơn hàng';
$lang['PROVISIONAL'] = 'Tạm tính';
$lang['INTO_MONEY'] = 'Thành tiền';


$lang['CUSTOMER_REVIEW'] = 'Khách hàng đánh giá';
$lang['BOUGHT_THIS_PRODUCT'] = 'Đã mua sản phẩm này tại Vipxchange.vn';
$lang['YOU_MUST_LOGIN_TO_COMMENT'] = 'Bạn phải đăng nhập để bình luận';
$lang['NO_COMMENT_YET'] = 'Chưa có bình luận';
$lang['RELATED_PRODUCT'] = 'Sản phẩm liên quan';
$lang['RECENTLY_VIEWED'] = 'Đã xem gần đây';
$lang['DELETE_HISTORY'] = 'Xóa lịch sử';
$lang['NO_PRODUCT'] = 'Chưa có sản phẩm';
$lang['SIZE_GUIDE'] = 'Hướng dẫn kích thước';



//MODAL SIZE
$lang['SIZE_GUIDE'] = 'Biểu đồ kích thước';
$lang['LENGTH'] = 'Chiều dài';
$lang['MEASURE_WAIST'] = 'Số đo vòng eo';
$lang['ROUND_BOTTOM'] = 'Vòng đáy';
$lang['SLEEVE_LENGTH'] = 'Chiều dài tay áo';
$lang['WRIST'] = 'Cổ tay';
$lang['LENGTH_SUBMERGED_ZIPPER'] = 'Độ dài dây kéo chìm';
$lang['UNIT'] = 'Đơn vị';
$lang['HOW_MEASURE'] = 'Cách đo';
$lang['CHEST_RING'] = 'Vòng ngực';
$lang['CHEST_RING_CONTENT'] = 'Giữ một điểm cố định rồi cho thước vào dây đi 1 vòng qua đỉnh ngực ra phía sau lưng đến điểm cố định';
$lang['WAIST'] = 'Vòng eo';
$lang['WAIST_CONTENT'] = 'Vòng eo là đường cong cách rốn 3-4cm, sau khi xác định được vị trí. Hãy đo vòng eo như bước 1';
$lang['BUTTOCKS'] = 'Vòng mông';
$lang['BUTTOCKS_CONTENT'] = 'Vòng mông hãy đo 1 vòng quanh mông, lưu ý phải đi qua đỉnh cao nhất của mông';
$lang['LONG_PANTS'] = 'Dài quần';
$lang['LONG_PANTS_CONTENT'] = 'Lấy từ điểm cong của eo giữ thẳng thước dây cho song song với chân';
$lang['LONG_BODY'] = 'Chiều dài cơ thể';
$lang['LONG_BODY_CONTENT_1'] = 'Dài thân được tính từ trên vai để sát gần cổ và cho thước dây đi qua điểm ngực và giữa cho thước dây song song với cơ thể';
$lang['LONG_BODY_CONTENT_2'] = 'Dài thân có thể tới mắc cá chân hoặc trên mắc cá chân tùy theo kiểu';
$lang['DELETE_HISTORY_SUCCESS'] = 'Xóa lịch sử thành công';
$lang['DELETE_HISTORY_ERROR'] = 'Xóa lịch sử thất bại';


//ADDRESS BOOK
$lang['ADDRESS_BOOK'] = 'Sổ địa chỉ';
$lang['EDIT_ADDRESS_SUCCESS'] = 'Cập nhật địa chỉ thành công';
$lang['EDIT_ADDRESS_ERROR'] = 'Cập nhật địa chỉ thất bại';
$lang['ADD_NEW_ADDRESS'] = 'Thêm địa chỉ mới';
$lang['ADD_NEW_ADDRESS_SUCCESS'] = 'Thêm địa chỉ mới thành công';
$lang['ADD_NEW_ADDRESS_ERROR'] = 'Thêm địa chỉ mới thất bại';

$lang['DELETE_ADDRESS_SUCCESS'] = 'Xóa địa chỉ thành công';
$lang['DELETE_ADDRESS_ERROR'] = 'Xóa địa chỉ thất bại';
$lang['EDIT_ADDRESS'] = 'Chỉnh sửa địa chỉ';
$lang['EDIT'] = 'Chỉnh sửa';
$lang['DELETE'] = 'Xóa';
$lang['LOAD_ADDRESS_BOOK_FAILED'] = 'Tải danh sách địa chỉ thất bại';
$lang['SELECT_CITY'] = 'Chọn Tỉnh/Thành phố';
$lang['SELECT_DISTRICT'] = 'Chọn Quận/Huyện';
$lang['WARD'] = 'Phường/Xã';
$lang['SELECT_WARD'] = 'Chọn Phường/Xã';
$lang['EXAMPLE_ADDRESS'] = 'Vd: 18 Lê Thánh Tông';
$lang['SET_DEFAULT_ADDRESS'] = 'Đặt địa chỉ mặc định';
$lang['SET_DEFAULT_ADDRESS_SUCCESS'] = 'Đặt địa chỉ mặc định thành công';
$lang['SET_DEFAULT_ADDRESS_ERROR'] = 'Đặt địa chỉ mặc định thất bại';
$lang['CELLULAR_PHONE'] = 'Điện thoại di động';
$lang['GET_DISTRICT_ERROR'] = 'Lấy danh sách huyện thất bại';
$lang['GET_WARD_ERROR'] = 'Lấy danh sách thị xã thất bại';
$lang['ADDRESS_INVALID'] = 'Vui lòng chọn/nhập đủ thông tin';
$lang['SET_DEFAULT_ADDRESS'] = 'Địa chỉ mặc định';
$lang['NO_DEFAULT_ADDRESS_FOUND'] = 'Không tìm thấy địa chỉ mặc định';


$lang['REQUEST_FAILED'] = 'Yêu cầu thất bại';


// Collection
$lang['PRICE_RANGE'] = 'Khoảng giá';
$lang['OTHER'] = 'Khác';
$lang['FILTER'] = 'Bộ lọc';
$lang['BUTTON_FILTER'] = 'Lọc';

//getNotification
$lang['NOTIFICATION'] = 'thông báo mới';
