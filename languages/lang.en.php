<?php
$lang = array();
// URL

$lang['SEARCH_URL'] = '/search';
$lang['CUSTOMER_URL'] = '/customer';
$lang['LOGIN_URL'] = '/login';
$lang['REGISTER_URL'] = '/register';
$lang['COLLECTION_ALL_URL'] = '/collection/all';
$lang['BLOG_ALL_URL'] = '/blog/all';
$lang['SEARCH_URL'] = '/search';
$lang['CONTACT'] = '/contact';
$lang['CUSTOMER_ORDER_URL'] = '/customer/order';
$lang['CUSTOMER_CHANGE_PASSWORD_URL'] = '/customer/change-password';
$lang['CUSTOMER_FORGOT_PASSWORD_URL'] = '/customer/forgot-password';
$lang['WISHLIST_URL'] = '/wishlist';
$lang['CART_URL'] = '/cart';
$lang['CUSTOMER_COUPON'] = '/customer/my-coupon';
$lang['CHECKOUT_URL'] = '/checkout';
$lang['CHECKOUT_SUCCESS_URL'] = '/order-success';
$lang['ADDRESS_BOOK_URL'] = '/customer/address-book';




//customer url
$lang['EDIT_ACCOUNT_URL'] = '/customer';
$lang['MY_ORDER_URL'] = '/customer/order';
$lang['ADDERSS_BOOK_URL_2'] = '/customer/address-book';
$lang['MY_WISHLIST_URL'] = '/customer/wishlist';
$lang['COUPON_URL'] = '/customer/coupon';



$lang['HOME'] = 'Home';
$lang['SUPPORT'] = 'Support';
$lang['CART'] = 'Cart';
$lang['NICART'] = 'There are no products in your shopping cart';
$lang['CHECKOUT'] = 'Checkout';
$lang['CHECKOUTS'] = 'Order success';
$lang['PRODUCT'] = 'Product';
$lang['AMOUNT'] = 'Amount';
$lang['TOTAL'] = 'Total';
$lang['NAME'] = 'Name';
$lang['PHONE'] = 'Phone';
$lang['CITY'] = 'City';
$lang['DISTRICT'] = 'District';
$lang['ADDRESS'] = 'Address';
$lang['CONTENT'] = 'Note';
$lang['ERROR'] = 'Error';
$lang['BTHOME'] = 'Back to home';
$lang['SEARCHH'] = 'Enter your search term';
$lang['SERCATE'] = 'Service Categories';
$lang['SUBSE'] = 'Enter your email address';
$lang['SUBS'] = 'Subscribe now!';
$lang['TITLE'] = 'Subject';
$lang['NAMET'] = 'Type your name';
$lang['EMAILT'] = 'Type your email';
$lang['PHONET'] = 'Type your phone';
$lang['CONTENTT'] = 'Type your message';
$lang['SEND'] = 'Send contact';
$lang['TALKPRO'] = 'Lets Talk About The Project';
$lang['BY'] = 'By';
$lang['BACK'] = 'Back';
$lang['SHARE'] = 'Share';
$lang['CATEGORY'] = 'Category';
$lang['SEENPOST'] = 'Featured Posts';
$lang['DEVBY'] = 'Develop by';
$lang['MOTFR'] = 'Monday - Friday';
$lang['SATURDAY'] = 'Saturday';
$lang['SUNDAY'] = 'Sunday';
$lang['MAILAT'] = 'Mail at';
$lang['CALLUS'] = 'Call us';
$lang['SEARCH'] = 'Search';
$lang['KEY_WORD'] = 'Key word';
$lang['STOTAL'] = 'Subtotal';
$lang['SEEM'] = 'See more';
$lang['DAY'] = 'Day';
$lang['MONTH'] = 'Month';
$lang['YEAR'] = 'Year';
$lang['SPLAN'] = 'Select Plan';
$lang['ATCART'] = 'Add to cart';
$lang['ALL'] = 'All';
$lang['AVAIL'] = 'Availability';
$lang['INSTOCK'] = 'In Stock';
$lang['NOSTOCK'] = 'No in Stock';
$lang['DESC'] = 'Description';
$lang['INFO'] = 'Information';
$lang['COMM'] = 'Reviews';
$lang['SEENPRO'] = 'Related Products';
$lang['HAVE'] = 'HAVE';
$lang['SEARCHR'] = 'Search results for';
$lang['SEARCHN'] = 'No search results for';
$lang['SEARCHNONE'] = 'Please try again with another keyword';
$lang['SEARCHNONE_FILTER'] = 'No results';
$lang['SEARCHN_FILTER'] = 'Please try again with another filter';
$lang['RESULTS'] = 'Search results';




$lang['ABOUT_US'] = 'About us';
$lang['SEARCH'] = 'Search';
$lang['SEE_MORE'] = 'See more';
$lang['RESULT_COMPETITION'] = 'RESULTS OF COMPETITION CLENDAR';
$lang['CATEGORY'] = 'Category';
$lang['ADDRESS'] = 'ADDRESS';
$lang['GET_INFO'] = 'Contact information';
$lang['NAME'] = 'Name';
$lang['EMAIL'] = 'Email';
$lang['PHONE'] = 'Phone';
$lang['MESSAGE'] = 'Message';
$lang['SUBMIT'] = 'Submit';
$lang['CONTACT_US'] = 'Contact Us';
$lang['INPUT_NAME'] = 'Name is Empty!';
$lang['INPUT_ADDRESS'] = 'Address is Empty!';
$lang['INPUT_EMAIL'] = 'Email is Empty!';
$lang['INPUT_PHONE'] = 'Phone is Empty!';
$lang['INPUT_REGION'] = 'You have not selected a Province / City';
$lang['INVALID_EMAIL'] = 'Email is Invalid!';
$lang['INVALID_PHONE'] = 'Phone is Invalid!';
$lang['EMPTY_CONTENT'] = 'Message is Empty!';
$lang['CONTACT_EARLY'] = 'We will contact you soon!';
$lang['VIEW_NOW'] = 'View more';
$lang['VIEW_PRODUCT_DETAL'] = 'View product detail';
$lang['ABOUT'] = 'ABOUT';
$lang['BUY_NOW'] = 'Buy now';
$lang['ADD_TO_CART'] = 'Add to cart';
$lang['SIZE_CHART'] = 'Size chart';
$lang['QUANTITY'] = 'Quantity';

$lang['HOME_PAGE'] = 'Home';
// USER
$lang['USER_PAGE'] = 'User page';
$lang['USER_INFO'] = 'User infomation';
$lang['CHANGE_PASS'] = 'Change password';
$lang['FULL_NAME'] = 'Full Name';
$lang['PHONE'] = 'Phone';
$lang['BIRTHDAY'] = 'Birthday';
$lang['GENDER'] = 'Gender';
$lang['MALE'] = 'Male';
$lang['FEMALE'] = 'Female';
$lang['OLD_PASS'] = 'Old password';
$lang['NEW_PASS'] = 'New password';
$lang['RE_PASS'] = 'Retype password';
$lang['BUTTON_SAVE'] = 'Save';
$lang['BUTTON_UPDATE'] = 'Update';
$lang['VIEW_BLOG'] = 'VIEW BLOG';
$lang['SUBSCRIBE'] = 'SUBSCRIBE';
$lang['DOWNLOAD_APP'] = 'Download app';
$lang['REGISTER_EMAIL'] = 'Register';
$lang['PLACEHOLDER_SUBSCIBE_EMAIL'] = 'Input your email';
$lang['CONNECT_US'] = 'Connect us';
$lang['WE_ACCEPT'] = 'We accept';
$lang['SHIPPING_PARTNER'] = 'Shipping partner';
$lang['NOTI_PASS_OLD'] = 'Please enter old password';
$lang['NOTI_PASS_NEW'] = 'Please enter new password!';
$lang['NOTI_PASS_VALIDATE'] = 'Password characters including numbers and letters!';
$lang['NOTI_PASS_PRE'] = 'Password not match!';
$lang['NOTI_PASS_SUCCESS'] = 'Update password success !';
$lang['NOTI_PASS_ERR'] = 'Update password error !';
// LOGIN
$lang['NOTI_LOGIN_SUCCESS'] = 'Login success !';
$lang['NOTI_LOGIN_ERR'] = 'Login fail !';
$lang['LOGIN_FB'] = 'Login with facebook';
$lang['LOGIN_GG'] = 'Login with google';
$lang['PASSWORD'] = 'Password';
$lang['REMEMBER_PASS'] = 'Remember';
$lang['FORGOT_PASS'] = 'Forgot Password ?';
$lang['LOGIN'] = 'Login';
$lang['NO_ACCOUNT'] = 'No account?';
$lang['HAVE_ACCOUNT'] = 'Have account?';
$lang['REGISTER'] = 'Register';

//header
$lang['ITEM_CART'] = 'items in cart';
$lang['SUM_CART'] = 'Sum';
$lang['VIEW_CART'] = 'View cart';
$lang['CHECKOUT_CART'] = 'Checkout';
$lang['EMPTY_CART'] = 'no items in cart';


//USER

$lang['EDIT_ACCOUNT'] = 'Edit account';
$lang['MY_ORDER'] = 'My order';
$lang['ADDRESS_BOOK'] = 'Address Book';
$lang['WISHLIST'] = 'WishList';
$lang['MY_COUPON'] = 'My coupon';
$lang['LOGOUT'] = 'Logout';

//product
$lang['SIZE'] = 'Size';
$lang['PRICE'] = 'Price';
$lang['COLOR'] = 'Color';
$lang['PARAMETER'] = 'Paramater';
$lang['WEIGHT'] = 'Weight';
$lang['CLOTH'] = 'Cloth';
$lang['SEASON'] = 'Season';
$lang['AGE'] = 'Age';
$lang['STYLE'] = 'Style';

$lang['CHANGE'] = 'Change';
$lang['REMOVE_CART'] = 'Remove';

//SUCCESS
$lang['PURCHASE_SUCCESS'] = 'Purchase success';
$lang['YOUR_ORDER_ID'] = 'Your order id is';
$lang['PURCHASE_SUCCESS_CONTENT_1'] = 'Vipxchange is happy to announce the';
$lang['PURCHASE_SUCCESS_CONTENT_2'] = 'order that has been received and is being processed. Order details have been sent to your email address.';
$lang['PURCHASE_SUCCESS_CONTENT_3'] = 'Orders will be delivered within 4 - 7 business days, depending on your destination.';

$lang['YOUR_QUESTION'] = 'Do you have questions to answer?';
$lang['FREQUENTLY_ASKED_QUESTION'] = 'Frequently asked questions';
$lang['SHIPPING_POLICY'] = 'Shipping policy';

$lang['OR_SEND_TO_EMAIL'] = 'Or send an email to';
$lang['TO_SUPPORT'] = 'for support';
$lang['DOWNLOAD_APP_TO_CHECK'] = 'Download the Vipxchange App to check orders anytime, anywhere';
$lang['GOTO_HOME_PAGE'] = 'Return to the home page';
//CHECKOUT
$lang['ADD_TO_CART_SUCCESS'] = 'Add to cart success!';
$lang['OUT_OF_STOCK'] = 'Out of stock';
$lang['ADD_TO_CART_OUT'] = 'Having passed the quantity in stock, please choose the product quantity again or choose another product';
// $lang['COLOR'] = 'Màu sắc';

//WISHLIST
$lang['ADD_WISHLIST_SUCCESS'] = 'Add product to wishtlist success!';
$lang['ADD_WISHLIST_ERROR'] = 'User not logged in!';
$lang['ADD_WISHLIST_EXIST'] = 'The product already exists in the favorites list!';

$lang['REMOVE_WISHLIST_SUCCESS'] = 'Remove the product from wishtlist success';
$lang['NO_WISHLIST_PRODUCT'] = 'No favorite products';
// $lang['COLOR'] = 'Màu sắc';

//ORDER
$lang['SECURE_PAYMENT'] = 'Secure payment';
$lang['SAVE'] = 'Save';
$lang['SAVE_INFORMATION'] = 'Save information successfully';
$lang['NAME_INFORMATION'] = 'Name and email are not empty';

$lang['SHIPPING_ADDRESS'] = 'Shipping address';
$lang['REGION'] = 'Province / City';
$lang['SUBREGION'] = 'District';
$lang['WARDS'] = 'Wards';
$lang['DEFAULT_ADDRESS'] = 'Set as default address';
$lang['PAYMENT_METHODS'] = 'Payment methods';
$lang['ORDER_SUMMARY'] = 'Order summary';
$lang['EDIT'] = 'Edit';
$lang['TRANSPORT_FEE'] = 'Transport fee';
$lang['EST_TIME'] = 'Estimated delivery time';

$lang['DETAIL'] = 'DETAIL';
$lang['BLOG'] = 'BLOG';
$lang['COMMENT'] = 'Comment';
$lang['VIEW_ALL'] = 'View all';
$lang['SEND_COMMENT'] = 'Send comment';
$lang['COMMENT_SUCCESS'] = 'Send comment success';
$lang['COMMENT_ERROR'] = 'Send comment failed';
$lang['WRITE_YOUR_COMMENT'] = 'Write your comment';
$lang['PLEASE_WRITE_COMMENT'] = 'Please write your comment';



$lang['CODE_ORDER'] = 'Order code';
$lang['TIME_ORDER'] = 'Time order';
$lang['STATUS_ORDER'] = 'Status';
$lang['TOTAL_MONEY'] = 'Total money';

$lang['COUPON'] = 'Coupon';


$lang['VALUE'] = 'Value';
$lang['TIME_RECEIVE'] = 'Time receive';
$lang['TIME_EXPIRED'] = 'Time exprired';


//STATUS ORDER
$lang['PROCESSING'] = 'Processing';
$lang['NEW'] = 'New';
$lang['CONFIRM'] = 'Confirm';
$lang['DONE'] = 'Done';
$lang['CANCEL'] = 'Cancel';
$lang['RETURN'] = 'Return';

//STATUS COUPON
$lang['NOTUSE'] = 'Not use';
$lang['EXPIRED'] = 'Expired';
$lang['USED'] = 'Used';

//  COUPON

$lang['COUPON_SUCCESS'] = 'Enter the successful coupon code!';
$lang['COUPON_ERR'] = 'Coupon code is incorrect or expired!';
$lang['CANCEL_COUPON_SUCCESS'] = 'Cancel successful coupon code!';
$lang['ORDER_NOW'] = 'Order now';
$lang['APPLY'] = 'Apply';
$lang['COUPON_USER'] = 'Using coupon code';
$lang['COUPON_CANCLE'] = 'Cancel coupon code';
$lang['COUPON_PLACE_HOLDER'] = '1 discount code / Each order';
$lang['ORDER_SUMMARY'] = 'Order summary';
$lang['PROVISIONAL'] = 'Provisional';
$lang['INTO_MONEY'] = 'Into money';

$lang['CUSTOMER_REVIEW'] = 'Customer review';
$lang['BOUGHT_THIS_PRODUCT'] = 'Bought this product at Vipxchange.vn';
$lang['YOU_MUST_LOGIN_TO_COMMENT'] = 'You must login to comment';
$lang['NO_COMMENT_YET'] = 'No comment yet';
$lang['RELATED_PRODUCT'] = 'Related product';
$lang['RECENTLY_VIEWED'] = 'Recently viewed';
$lang['DELETE_HISTORY'] = 'Delete history';
$lang['NO_PRODUCT'] = 'No product';
$lang['FOLLOW_US'] = 'Follow us';
$lang['PAGE_CONTACT'] = 'Contact';
//MODAL SIZE
$lang['SIZE_GUIDE'] = 'Size guide';
$lang['LENGTH'] = 'Length';
$lang['MEASURE_WAIST'] = 'Measure the waist';
$lang['ROUND_BOTTOM'] = 'Round bottom';
$lang['SLEEVE_LENGTH'] = 'Sleeve lenght';
$lang['WRIST'] = 'Wrist';
$lang['LENGTH_SUBMERGED_ZIPPER'] = 'Length of submerged zipper';
$lang['UNIT'] = 'Unit';
$lang['HOW_MEASURE'] = 'How to measure';
$lang['CHEST_RING'] = 'Chest ring';
$lang['CHEST_RING_CONTENT'] = 'Hold a fixed point and then put the ruler into the rope 1 round the top of the chest behind the back to the fixed point';
$lang['WAIST'] = 'Waist';
$lang['WAIST_CONTENT'] = 'Waistline is a curve of 3-4cm in the navel, after determining the position. Measure your waist like step 1';
$lang['BUTTOCKS'] = 'Buttocks';
$lang['BUTTOCKS_CONTENT'] = 'Make sure your buttocks measure 1 around your butt, so be sure to go over the top of the buttock';
$lang['LONG_PANTS'] = 'Long pants';
$lang['LONG_PANTS_CONTENT'] = 'From the waist point of the waist, hold the tape straight for the leg';
$lang['LONG_BODY'] = 'Long body';
$lang['LONG_BODY_CONTENT_1'] = 'The body length is calculated from the shoulder to the neck and the tape measure passes through the chest point and the middle of the tape is parallel to the body';
$lang['LONG_BODY_CONTENT_2'] = 'The body length may come from the ankles or the ankles depending on the type';
$lang['DELETE_HISTORY_SUCCESS'] = 'Delete history successs';
$lang['DELETE_HISTORY_ERROR'] = 'Delete history error';


//ADDRESS BOOK
$lang['ADDRESS_BOOK'] = 'Address book';
$lang['ADD_NEW_ADDRESS'] = 'Add new address';
$lang['ADD_NEW_ADDRESS_SUCCESS'] = 'Add new address success';
$lang['ADD_NEW_ADDRESS_ERROR'] = 'Add new address failed';
$lang['DELETE_ADDRESS_SUCCESS'] = 'Delete address success';
$lang['DELETE_ADDRESS_ERROR'] = 'Delete address failed';


$lang['EDIT_ADDRESS'] = 'Edit address';
$lang['EDIT_ADDRESS_SUCCESS'] = 'Edit address success';
$lang['EDIT_ADDRESS_ERROR'] = 'Edit address failed';
$lang['EDIT'] = 'Edit';
$lang['DELETE'] = 'Delete';
$lang['LOAD_ADDRESS_BOOK_FAILED'] = 'Load address book failed';
$lang['SELECT_CITY'] = 'Select city';
$lang['SELECT_DISTRICT'] = 'Select district';
$lang['WARD'] = 'Ward';
$lang['SELECT_WARD'] = 'Select ward';
$lang['EXAMPLE_ADDRESS'] = 'Ex: 18 Lê Thánh Tông';
$lang['SET_DEFAULT_ADDRESS'] = 'Set default address';
$lang['CELLULAR_PHONE'] = 'Cellular phone';
$lang['SET_DEFAULT_ADDRESS_SUCCESS'] = 'Set default address success';
$lang['SET_DEFAULT_ADDRESS_ERROR'] = 'Set default address failed';

$lang['GET_DISTRICT_ERROR'] = 'Get list district failed';
$lang['GET_WARD_ERROR'] = 'Get list ward failed';
$lang['ADDRESS_INVALID'] = 'Please select/enter enough information';
$lang['SET_DEFAULT_ADDRESS'] = 'Set default address';
$lang['NO_DEFAULT_ADDRESS_FOUND'] = 'No default address found';


$lang['REQUEST_FAILED'] = 'Request failed';

// Collection
$lang['PRICE_RANGE'] = 'Price';
$lang['OTHER'] = 'Other';
$lang['FILTER'] = 'Filter';
$lang['BUTTON_FILTER'] = 'Filter';

//getNotification
$lang['NOTIFICATION'] = 'new notification';
