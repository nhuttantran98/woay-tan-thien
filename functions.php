<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
header("Access-Control-Allow-Origin: *");

class CustomAdminController extends adminController {
  public function helloWorld(Request $request, Response $response) {
    return $response->write('Hello World');
  }
}

class CustomStoreFrontController extends Controller {
}

// TODO: add custom admin routes
function add_custom_admin_routes($app) {
}

// TODO: add custom store front routes
function add_custom_store_front_routes($app) {
}

/* CUSTOM FIELD

  registerCustomField($title, $post_type, $input_type, $default_value)
  + $title: Tiêu đề
  + $post_type: Đối tượng áp dụng
    product, article, collection, blog, page, gallery, photo
  + $input_type: Kiểu nhập liệu
    input, select, select-multiple, textarea, editor, number, datetime, image
  + $default_value: Giá trị mặc định (optional)
    - Nếu input_type là select, select-multiple thì giá trị mặc định phải là:
      i)  array: ['Option 1', 'Option 2'], hoặc
      ii) 1 trong các giá trị products, collections, blogs, articles, pages, regions
*/

registerCustomField("My Custom Field","product","editor");