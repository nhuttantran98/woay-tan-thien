<?php  
  $settings = array (
  'favicon' => 'woay_(11)_1562127857.png',
  'logo' => 'woay_(23)_1562133509.png',
  'background' => 'woay-bg_1562128273.png',
  'woay' => 'woay_1562317514.png',
  'popup_background' => '',
  'banner_top' => 'home-four-banner-background-image-1_1562208372.png',
  'banner_bottom' => 'home-four-banner-background-image-1_1562208372.png',
  'popup_message' => '',
  'facebook_url' => '',
  'content_description' => '',
  'contact_title' => 'Yosu',
  'contact_desc' => 'Phan Dinh Phung',
  'maps' => 
  array (
    'lat' => 10.7884672,
    'lng' => 106.63047670000003,
  ),
  'contact_meta_title' => '',
  'contact_meta_description' => '',
  'contact_meta_keyword' => '',
  'contact_meta_robots' => '',
  'contact_meta_image' => '',
  'meta_title' => '',
  'meta_description' => '',
  'meta_keyword' => '',
  'meta_robots' => '',
  'meta_image' => '',
  'google_analytics' => '',
  'facebook_pixel' => '',
  'chat_online' => '',
  'rewards' => '[{
  sku: \'FREESHIP\',
  title: \'Giao hàng miễn phí\',
  type: \'freeship\'
}, {
  sku: \'VOUCHER50%\',
  title: \'Voucher giảm giá 50%\',
  type: \'percentage\'
}, {
  sku: \'VOUCHER50K\',
  title: \'Voucher giảm giá 50%\',
  type: \'percentage\'
}, {
  sku: \'FREESHIP\',
  title: \'Giao hàng miễn phí\',
  type: \'freeship\'
}, {
  sku: \'COUPON50%\',
  title: \'COUPON giảm giá 50%\',
  type: \'percentage\'
}, {
  sku: \'GIFT\',
  title: \'Ưu đãi bất ngỡ\',
  type: \'gift\'
}, {
  sku: \'COUPON50K\',
  title: \'COUPON giảm giá 50K\',
  type: \'percentage\'
}];',
  'latlng' => '10.7884672,106.63047670000003',
  'pizza_count' => '7',
  'content_description_1' => '',
  'content_description_2' => '',
  'tab_1_title' => 'Thể lệ chương trình',
  'tab_1_description' => '<div class="copyright" style="box-sizing: border-box; color: #111111; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;"></div>
<div class="box-info-game" style="box-sizing: border-box; position: relative; color: #111111; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">
<div class="tab-content" style="box-sizing: border-box; background: #ffffff; padding: 20px;">
<div id="motachuongtrinh" class="tab-pane fade in active" style="box-sizing: border-box; opacity: 1; transition: opacity 0.15s linear 0s; display: block;">
<h3 style="box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 24px;">Chơi l&agrave; tr&uacute;ng, qu&agrave; tặng hấp dẫn</h3>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Thời gian bắt đầu: Ng&agrave;y 12/03/2019</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Thời gian kết th&uacute;c: 21h ng&agrave;y 15/3/2019</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Ch&agrave;o đ&oacute;n th&aacute;ng của một nửa Thế giới, Cỏ mềm gửi tặng bạn chương tr&igrave;nh mini game cực th&uacute; vị. Thật đơn giản, chỉ cần click QUAY ở giữa v&ograve;ng tr&ograve;n may mắn th&igrave; bạn sẽ l&agrave; người duy nhất c&oacute; cơ hội tr&uacute;ng IPHONE 7. B&ecirc;n cạnh đ&oacute;, bạn sẽ c&oacute; x&aacute;c suất 100% cơ hội tr&uacute;ng những phần thưởng gi&aacute; trị bao gồm tặng Bộ qu&agrave; tặng N&agrave;ng thơ cực ngọt ng&agrave;o, combo trị Mụn lưng hiệu quả, phiếu giảm gi&aacute; 20%/ 10% cho tất cả sản phẩm v&agrave; rất nhiều suất Freeship khi mua h&agrave;ng tr&ecirc;n to&agrave;n quốc</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Đặc biệt, chương tr&igrave;nh &ldquo;Quay l&agrave; tr&uacute;ng&ldquo; l&agrave; để tri &acirc;n kh&aacute;ch h&agrave;ng. V&igrave; vậy chương tr&igrave;nh &aacute;p dụng cho tất cả đối tượng v&agrave; ho&agrave;n to&agrave;n MIỄN PH&Iacute;, k&eacute;o d&agrave;i đến 21h ng&agrave;y 12/03/2019</p>
<h3 style="box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 24px;"></h3>
<h3 style="box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 24px;">Thể lệ tham gia cực đơn giản</h3>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Bước 1: Like v&agrave; Share b&agrave;i viết dưới chế độ c&ocirc;ng khai.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Bước 2: Tag t&ecirc;n 3 người bạn</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Bước 3: Truy cập minigame.comem.vn, click v&agrave;o giữa v&ograve;ng quay v&agrave; quay số.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Bước 4: Nhắn tin cho Cỏ về M&atilde; tr&uacute;ng thưởng, Cỏ mềm sẽ tiến h&agrave;nh trao thưởng.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Lưu &yacute;:</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">V&ograve;ng quay sẽ đ&oacute;ng v&agrave;o l&uacute;c 21h ng&agrave;y 12/3/2019.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">C&aacute;c phần qu&agrave; l&agrave; voucher sẽ c&oacute; gi&aacute; trị đến hết ng&agrave;y 20/3 v&agrave; kh&ocirc;ng &aacute;p dụng k&egrave;m c&aacute;c chương tr&igrave;nh khuyến m&atilde;i kh&aacute;c.</p>
<h3 style="box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 24px;"></h3>
<h3 style="box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 24px;">Cửa h&agrave;ng:</h3>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">TP. Hồ Ch&iacute; Minh: 56 V&otilde; Thị S&aacute;u, Quận 1</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">H&agrave; Nội: Số 1 Ho&agrave;ng Cầu, Đống Đa &amp; 225 Trần Đăng Ninh, Cầu Giấy</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Hotline: 0968 622 511</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Phản hồi sản phẩm:<span>&nbsp;</span><a href="http://bit.ly/1UgEiCz" target="_blank" style="box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;" rel="noopener">http://bit.ly/1UgEiCz</a></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;">Website:<span>&nbsp;</span><a href="https://comem.vn/" target="_blank" style="box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;" rel="noopener">https://comem.vn</a></p>
</div>
</div>
</div>',
  'tab_2_title' => 'Hướng dẫn sử dụng phần thưởng',
  'tab_2_description' => '<div class="copyright" style="box-sizing: border-box; color: #111111; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;"></div>
<div class="box-info-game" style="box-sizing: border-box; position: relative; color: #111111; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">
<div class="tab-content" style="box-sizing: border-box; background: #ffffff; padding: 20px;">
<div id="huongdansudung" class="tab-pane fade active in" style="box-sizing: border-box; opacity: 1; transition: opacity 0.15s linear 0s; display: block;">
<h3 style="box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 24px;">Hướng dẫn sử dụng m&atilde; tr&uacute;ng thưởng chương tr&igrave;nh "V&ograve;ng quay may mắn"</h3>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"></p>
<p style="text-align: justify;"><span style="color: #ff0000;"><strong style="box-sizing: border-box; font-weight: bold;">A. SỬ DỤNG M&Atilde; TẠI CỬA H&Agrave;NG</strong></span></p>
<p style="text-align: justify;"><strong style="box-sizing: border-box; font-weight: bold;">Bước 1:</strong>Chụp ảnh m&agrave;n h&igrave;nh tr&uacute;ng thưởng</p>
<p style="text-align: justify;"><strong style="box-sizing: border-box; font-weight: bold;">Bước 2:</strong>Đưa m&atilde; tr&uacute;ng thưởng cho thu ng&acirc;n tại cửa h&agrave;ng</p>
<p style="text-align: justify;"><strong style="box-sizing: border-box; font-weight: bold;"></strong></p>
<p style="text-align: justify;"><span style="color: #ff0000;"><strong style="box-sizing: border-box; font-weight: bold;">B. SỬ DỤNG M&Atilde; KHI ĐẶT H&Agrave;NG ONLINE</strong></span></p>
<p style="text-align: justify;"><strong style="box-sizing: border-box; font-weight: bold;">Bước 1:</strong>&nbsp;Bạn c&oacute; thể truy cập v&agrave;o website<span>&nbsp;</span><strong style="box-sizing: border-box; font-weight: bold;">www.woay.vn</strong>&nbsp;v&agrave; đặt h&agrave;ng.</p>
<p style="text-align: justify;">- Chọn từng danh mục sản phẩm để t&igrave;m kiếm</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"><img src="//file.hstatic.net/1000253775/file/hdcs_1024x1024.png" style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p style="text-align: justify;"></p>
<p style="text-align: justify;"><strong style="box-sizing: border-box; font-weight: bold;">Bước 2:</strong>&nbsp;Chọn sản phẩm cần mua</p>
<p style="text-align: justify;">- Sau khi t&igrave;m được sản phẩm cần mua,bạn tiến h&agrave;nh đặt h&agrave;ng hoặc nếu muốn mua th&ecirc;m c&aacute;c sản phẩm kh&aacute;c,bạn h&atilde;y th&ecirc;m sản phẩm v&agrave;o giỏ h&agrave;ng v&agrave; quay trở lại sản phẩm kh&aacute;c để chọn th&ecirc;m h&agrave;ng.</p>
<p style="text-align: justify;"><em style="box-sizing: border-box;">* Bạn cần chọn tất cả c&aacute;c sản phẩm cần đặt mua v&agrave;o giỏ h&agrave;ng v&agrave; tạo tr&ecirc;n 1 đơn h&agrave;ng.&nbsp;Tr&aacute;nh việc chọn v&agrave; tạo nhiều đơn lẻ,sẽ rất kh&oacute; khăn trong việc x&aacute;c nhận đơn h&agrave;ng cho bạn.</em></p>
<p style="text-align: justify;"><em style="box-sizing: border-box;">* Sản phẩm hết m&agrave;u,hết size sẽ c&oacute; gạch ch&eacute;o v&agrave; kh&ocirc;ng th&ecirc;m v&agrave;o giỏ h&agrave;ng được.</em></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"><img src="//file.hstatic.net/1000253775/file/hdcs_2_1024x1024.png" style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p style="text-align: justify;"></p>
<p style="text-align: justify;">- Tại mục Giỏ h&agrave;ng,bạn c&oacute; thể bấm n&uacute;t &shy; nếu muốn huỷ sản phẩm đ&atilde; chọn để mua sản phẩm kh&aacute;c.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"><img src="//file.hstatic.net/1000253775/file/hdcs_3_1024x1024.png" width="408" height="505" style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"><img src="//file.hstatic.net/1000253775/file/hdcs_4_1024x1024.png" style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p style="text-align: justify;">- Sau khi đ&atilde; chọn được sản phẩm cần mua,bạn bấm v&agrave;o n&uacute;t&nbsp;<strong style="box-sizing: border-box; font-weight: bold;">THANH TO&Aacute;N</strong>&nbsp;v&agrave; điền đầy đủ th&ocirc;ng tin c&aacute; nh&acirc;n v&agrave;o bảng th&ocirc;ng tin giao h&agrave;ng. Bạn cũng c&oacute; thể ghi ch&uacute; v&agrave;o đơn h&agrave;ng nếu bạn c&oacute; dặn d&ograve; hoặc c&oacute; th&ocirc;ng tin chưa r&otilde; về sản phẩm để shop tư vấn r&otilde; hơn.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><em style="box-sizing: border-box;">* Bạn cần nhập đầy đủ v&agrave; ch&iacute;nh x&aacute;c th&ocirc;ng tin để h&agrave;ng c&oacute; thể được giao đến đ&uacute;ng địa chỉ người mua h&agrave;ng,tr&aacute;nh việc thất lạc hoặc kh&ocirc;ng giao được đơn h&agrave;ng đến cho bạn.</em></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"><img style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"><img src="//file.hstatic.net/1000253775/file/hdcs_6_687dff4ce39c4380a846761947d87cb0_1024x1024.png" style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p style="text-align: justify;">- Chọn h&igrave;nh thức thanh to&aacute;n</p>
<p style="text-align: justify;">+ Thanh to&aacute;n khi nhận h&agrave;ng (COD)</p>
<p style="text-align: justify;">+ Chuyển khoản qua ng&acirc;n h&agrave;ng</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Nếu bạn c&oacute; m&atilde; giảm gi&aacute;,nhập m&atilde; v&agrave;o &ocirc; v&agrave; chọn n&uacute;t Sử dụng. Đơn h&agrave;ng sẽ được giảm theo m&atilde; giảm gi&aacute;.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"><img src="https://file.hstatic.net/1000253775/file/2019-06-06_12-48-36_d7e2a66c86654aefbc8eb7e04c7e1698_grande.jpg" width="454" height="387" style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p style="text-align: justify;">- Sau khi điền đầy đủ th&ocirc;ng tin v&agrave; kiểm tra lại đơn h&agrave;ng,gi&aacute; tiền,bạn h&atilde;y bấm v&agrave;o n&uacute;t&nbsp;<strong style="box-sizing: border-box; font-weight: bold;">HO&Agrave;N TẤT ĐƠN H&Agrave;NG</strong>.</p>
<p style="text-align: justify;">Nh&acirc;n vi&ecirc;n shop sẽ li&ecirc;n hệ với bạn qua điện thoại để x&aacute;c nhận đơn h&agrave;ng v&agrave; th&ocirc;ng tin giao h&agrave;ng.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px;"></p>
</div>
</div>
</div>',
  'tab_3_title' => 'Kiểm tra phần thưởng',
  'copyright' => '',
  'banner_bottom_link' => 'https://www.google.com/',
);