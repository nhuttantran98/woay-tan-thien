<?php  
  $settings = array (
  'favicon' => '',
  'logo' => '',
  'background' => 'woay-bg_1562128273.png',
  'woay' => 'woay_(23)_1562133509.png',
  'popup_background' => '',
  'banner_top' => '',
  'banner_bottom' => '',
  'popup_message' => '',
  'facebook_url' => '',
  'content_description' => '',
  'contact_title' => 'Yosu',
  'contact_desc' => 'Phan Dinh Phung',
  'maps' => 
  array (
    'lat' => 10.7884672,
    'lng' => 106.63047670000003,
  ),
  'contact_meta_title' => '',
  'contact_meta_description' => '',
  'contact_meta_keyword' => '',
  'contact_meta_robots' => '',
  'contact_meta_image' => '',
  'meta_title' => '',
  'meta_description' => '',
  'meta_keyword' => '',
  'meta_robots' => '',
  'meta_image' => '',
  'google_analytics' => '',
  'facebook_pixel' => '',
  'chat_online' => '',
  'rewards' => '[{
    sku: \'COUPON20%\',
    title: \'Coupon giảm giá 20%\',
    type: \'percentage\'
}, {
    sku: \'COUPON10%\',
    title: \'Coupon giảm giá 10%\',
    type: \'percentage\'
}, {
    sku: \'COUPON5%\',
    title: \'Coupon giảm giá 5%\',
    type: \'percentage\'
}, {
    sku: \'GOODLUCK\',
    title: \'Chúc bạn may mắn lấn sau\',
    type: \'fail\'
}, {
    sku: \'M-100k\',
    title: \'Coupon trị giá 100,000đ\',
    type: \'cash\'
}, {
    sku: \'M-200k\',
    title: \'Coupon trị giá 200,000đ\',
    type: \'cash\'
}];
',
  'latlng' => '10.7884672,106.63047670000003',
  'pizza_count' => '9',
  'content_description_1' => '',
  'content_description_2' => '',
);