var gulp = require('gulp');
var sass = require('gulp-sass');
var minify = require('gulp-minify');
var minifyCss = require('gulp-clean-css');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var livereload = require('gulp-livereload');

var themeDir = './';

// theme
gulp.task('scss', function () {
    gulp.src(themeDir + 'scss/style.scss')
        .pipe(
            sass({
                outputStyle: 'expanded',
                sourceComments: false
            }).on('error', sass.logError)
        )
        .pipe(gulp.dest('css/'))
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('css/'))
        .pipe(livereload());
});

// gulp.task('js', function() {
//   gulp.src(themeDir + 'js/script.js')
//     .pipe(minify())
//     .pipe(gulp.dest(themeDir + 'js/'))
// });
//


gulp.task('default', function () {
    // gulp.watch('public/static/scss/admin.scss',['admin']);
    gulp.start('scss');
    gulp.watch(themeDir + 'scss/*.scss', ['scss']);
    // gulp.watch(themeDir + 'js/script.js',['js']);
});

gulp.task('build', ['scss']);
